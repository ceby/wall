#!/usr/bin/env python3

import argparse
import atexit
import re
import shlex
import sys
import time
import webbrowser
from urllib import request, parse

class Facebook:
	min_period = 1
	warn_sleep = 0
	captcha_sleep = 0

	last_request_time = 0
	request_counts = {}

	@staticmethod
	def load(message, url, data=None, type=None, returl=False):
		while time.time() - Facebook.last_request_time < Facebook.min_period:
			time.sleep(max(0, Facebook.min_period - (time.time() - Facebook.last_request_time)))
		Facebook.last_request_time = time.time()

		message = message.replace('%u', url)
		if type:
			Facebook.request_counts[type] = Facebook.request_counts.get(type, 0) + 1
			message = message.replace('%c', str(Facebook.request_counts[type]))

		print(time.strftime('[%H:%M:%S]'), message + '... ', end='')
		sys.stdout.flush()
		page = Facebook.http.open(url, data)
		response = page.read()
		redirect = re.search(b'window.location.replace\\("(.*?)"\\)', response)
		if redirect:
			print('redirected!')
			return Facebook.load(message, redirect.group(1).decode().replace('\\', ''), data, returl)
		print('done.')
		return (page.geturl(), response) if returl else response

	@staticmethod
	def login():
		# setup cookie jar
		http = request.build_opener(request.HTTPCookieProcessor())
		http.addheaders = [('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64; rv:2.0.1) Gecko/20110429 Firefox/4.0.1')]
		Facebook.http = http

		# fetch initial facebook cookies (we can't login without these; they are used to
		# check if the browser supports cookies)
		Facebook.load('Loading Facebook main page', 'http://www.facebook.com/')

		# login to fetch facebook session cookies
		url, home = Facebook.load('Sending login request', 'https://www.facebook.com/login.php?refid=0',
			parse.urlencode((('email', Facebook.email), ('pass', Facebook.password))).encode(), returl=True)

		if url.find('checkpoint') != -1:
			print("Facebook needs your attention. This can happen for a number of reasons.")
			print("This login may look suspicious, and Facebook may require confirmation.")
			print("Login through your web browser to fix this, then run the script again.")
			print("You may have Facebook setup to require a confirmation code for new logins.")
			print("You will have to disable this option before using this script.")
			exit(-1)
		if url.find('setup_machine.php') != -1:
			print("You have Facebook setup to notify you of new logins via email, which this")
			print("script does not support. You will have to disable this option before using")
			print("this script.")
			exit(-1)
		# we were redirected to the login page; login must have failed
		elif url.find('login.php') != -1:
			print("Failed to log in.")
			print("Check your username and password and try again. If they are correct and the")
			print("script is still failing, Facebook may have been updated in a way that breaks")
			print("the script. Also note that Facebook will prevent you from logging in more than")
			print("a few times per minute. Be patient.")
			exit(-1)

		id = re.search(b'"viewer":(\\d+)', home)
		if id:
			Facebook.fbid = id.group(1)
		else:
			print('Warning: Failed to determine your Facebook id. This may cause problems.')

	@staticmethod
	def create_target(params):
		opts = shlex.split(re.search(b'\\[([^\\]]*)', params).group(1).decode())
		if len(opts) == 0:
			print('Failed to parse target ' + params + ': no parameters given.')
			exit(-1)

		if opts[0] == 'message':
			return MessageTarget(opts)
		elif opts[0] == 'comment':
			return CommentTarget(opts)
		elif opts[0] == 'wall':
			return WallTarget(opts)
		elif opts[0] == 'like':
			return LikeTarget(opts)
		elif opts[0] == 'opts':
			for opt in opts:
				if opt.startswith('warn_sleep:'):
					Facebook.warn_sleep = max(float(opt[11:]), 0)
				elif opt.startswith('captcha_sleep:'):
					Facebook.captcha_sleep = max(float(opt[14:]), 0)
				elif opt.startswith('min_period:'):
					Facebook.min_period = max(float(opt[11:]), 0)
		else:
			print('Unknown target: ' + line.decode())
			exit(-1)

	@staticmethod
	def run(config):
		# load settings
		print("Loading parameters from " + config)
		try:
			file = open(config, mode='rb')
		except IOError as e:
			print(e)
			print()
			print("You should create a file named '" + config + "' in the following format:")
			print("your_email@example.com")
			print("your_password")
			print("[target]")
			print("comment one")
			print("comment two")
			print("...")
			print("[another target]")
			print("...")
			print()
			print("Here are some available targets:")
			print("# Post wall messages")
			print("[wall http://facebook.com/wall]")
			print("# Post wall messages to a friend")
			print("[wall 'Joe Friend']")
			print("# Post wall messages, like them, then delete them. (Creates notification spam)")
			print("[wall http://facebook.com/wall like stealth]")
			print("# Comment on a status (You can get a link to a status by clicking on the time)")
			print("[comment http://www.facebook.com/username/posts/0000000000000]")
			print("# Comment on a photo (or any page that has 'Write a comment...')")
			print("[comment http://www.facebook.com/photo.php?fbid=000...]")
			print("# Send a message to a friend")
			print("[message 'Joe Friend']")
			print("# Send a message to anyone")
			print("[message http//www.facebook.com/username]")
			print("# Specify a message subject by separating it from the body with a tab, e.g.")
			print("subject goes here	body goes here")
			print("# Like everything on a wall (this target ignores any comments given)")
			print("[like http://facebook.com/wall]")
			print("# Like everything on a wall up to the first liked item")
			print("[like http://facebook.com/wall halt]")
			print("# Like things on a wall, avoiding items that create notifications for others")
			print("[like http://facebook.com/wall limited]")
			print("# Like things on a wall relating to a particular person")
			print("[like http://facebook.com/wall restrict:'Joe Friend']")
			print("[like http://facebook.com/wall restrict:http://facebook.com/other]")
			print("# Like everything on a wall, even already liked items (to create notifications)")
			print("[like http://facebook.com/wall relike]")
			print("# Specify options (will apply to all following targets)")
			print("[opts option:value ...]")
			print("# Available options:")
			print("# min_period:n  minimum time in seconds between HTTP requests (defaults to")
			print("#               to 1.0)")
			print("# warn_sleep:n  interval in seconds to wait when Facebook suspects abusive")
			print("#               behavior (defaults to 0, which means prompt to continue)")
			print("# captcha_sleep:n  interval in seconds to wait when Facebook sends a captcha")
			print("#                  (defaults to 0, which means prompt for the captcha answer)")
			exit(-1)

		Facebook.email = file.readline().decode().rstrip()
		Facebook.password = file.readline().decode().rstrip()

		print("Will post with account:", Facebook.email)
		print()

		Facebook.login()

		target = None
		for line in file:
			line = line.strip()
			if line.startswith(b'#'):
				pass
			elif line.startswith(b'['):
				target = Facebook.create_target(line)
			elif target:
				target.post(line)
			else:
				print("Error reading " + config + ": found comment '" + line.decode() + "' before any targets.")
				exit(-1)

	@staticmethod
	def load_friends():
		if hasattr(Facebook, 'friends'):
			return Facebook.friends

		Facebook.friends = Facebook.load('Loading list of friends', 'https://www.facebook.com/ajax/typeahead_friends.php?{0}-{1}-6&u={0}&include_me=1&lists=1&include_emails=1&__a=1'.format(Facebook.fbid.decode(), int(time.time())))
		return Facebook.friends

	@staticmethod
	def parse_input_tags(form):
		# parse input tag data into form-urlencoded format
		return b'&'.join([ k + b'=' + v for k, v in re.findall(b'input[^>]*name=\\\\*"(?!charset_test)(?!next_pa)([^"]*?)\\\\*"[^>]*value=\\\\*"([^"]*?)\\\\*"', form) ]).replace(b'&quot;', b'"').replace(b'&#125;', b'}').replace(b'&#123;', b'{').replace(b'&amp;', b'%26')

	@staticmethod
	def find_fbid(wall):
		id = re.search(b'composer.php\\?id=([0-9]+)', wall)
		if id:
			return id.group(1)

		print('Unable to determine Facebook id from profile. Does the page contain a "Send')
		print('Message" button?')
		exit(-1)

	@staticmethod
	def lookup_fbid(input):
		if re.match('^[0-9]+$', input):
			# looks like we already have a Facebook id
			return input.encode()

		friends = Facebook.load_friends()
		id = re.search(b'(?i)' + re.escape(input.encode()) + b'","i":(\\d+)', friends)
		if id:
			return id.group(1)

		print('Could not find ' + input + '. This script only searches your friends')
		print('when a name is provided. Try giving a profile url instead.')
		exit(-1)

	@staticmethod
	def lookup_load_fbid(input):
		if input.startswith('http'):
			return Facebook.find_fbid(Facebook.load('Loading profile to determine Facebook id', input))
		return Facebook.lookup_fbid(input)

	@staticmethod
	def lookup_wall_url(input):
		if input.startswith('http'):
			return input
		return 'http://www.facebook.com/profile.php?sk=wall&id=' + Facebook.lookup_fbid(input).decode()

	@staticmethod
	def captcha_input(url):
		print('Go to', url, 'to view the captcha.')
		if Facebook.open_browser:
			webbrowser.open_new_tab(url)
		print('Type the captcha text and press enter to submit. You should probably wait a few')
		print('minutes before submitting, otherwise Facebook will immediately give you another')
		print('captcha.')
		return parse.quote(input('Enter the captcha text (or Ctrl-C to abort): ')).encode()

	@staticmethod
	def parse_response(response):
		if response.find(b'"error":') != -1:
			# Ideally we would determine the threshold for abusive
			# behavior and prevent the script from crossing it,
			# rather than require the user to deal with it...
			if response.find(b'"error":1390004') != -1:
				print('Facebook suspects you of "abusive behavior" and wants you to slow down (they')
				print('threaten to disable your account if you don\'t).')
				if Facebook.warn_sleep == 0:
					input('Press enter when you feel safe to continue (or Ctrl-C to abort)...')
				else:
					print('Sleeping for', Facebook.warn_sleep, 'seconds')
					time.sleep(Facebook.warn_sleep)
			elif response.find(b'"error":1357007') != -1:
				print('Facebook suspects you of "abusive behavior" and wants you to slow down and enter')
				print('a captcha.')
				if Facebook.captcha_sleep != 0:
					print('Sleeping for', Facebook.captcha_sleep, 'seconds')
					time.sleep(Facebook.captcha_sleep)
					return

				data = Facebook.parse_input_tags(response)

				url = re.search(b'src=\\\\"(.*?captcha_challenge_code.*?)\\\\" ', response)
				if url:
					return data + b'&confirmed=1&recaptcha_challenge_field=&captcha_response=' + Facebook.captcha_input(url.group(1).replace(b'\/', b'/').replace(b'&amp;', b'&').decode())
				else:
					captcha = Facebook.load('Loading captcha info', 'https://www.google.com/recaptcha/api/challenge?k=6LezHAAAAAAAADqVjseQ3ctG3ocfQs2Elo1FTa_a&ajax=1&' + data.decode())
					key = re.search(b'challenge : \'([^\']+)\'', captcha)
					if key:
						key = key.group(1)
						return b'confirmed=1&' + data + b'&recaptcha_challenge_field=' + key + b'&captcha_response=' + Facebook.captcha_input('https://www.google.com/recaptcha/api/image?c=' + key.decode())
					else:
						print('Error loading captcha info.')
			else:
				print("Request failed! Response was:")
				print(response)
				exit(-1)

class Target:
	def __init__(self, opts):
		self.opts = opts

	def post_data(self, message):
		pass

	def like_item(self, response):
		pass

	def delete_item(self, response):
		pass

	def post(self, message, captcha=None):
		# post the data to the page
		type, url, data = self.post_data(re.sub(b'&', b'%26', message))
		if captcha:
			response = Facebook.load("Resending '" + message.decode() + "' with captcha (" + type + " #%c)", url, data + b'&' + captcha, type)
		else:
			response = Facebook.load("Sending '" + message.decode() + "' (" + type + " #%c)", url, data, type)

		captcha = Facebook.parse_response(response)
		if captcha:
			self.post(message, captcha)
			return

		if 'like' in self.opts:
			self.like_item(response)
		if 'stealth' in self.opts:
			self.delete_item(response)

class MessageTarget(Target):
	def __init__(self, opts):
		super().__init__(opts)

		self.url = 'https://www.facebook.com/ajax/gigaboxx/endpoint/MessageComposerEndpoint.php?__a=1'
		self.data = MessageTarget.load_composer() + b'&ids[0]=' + Facebook.lookup_load_fbid(opts[1])

	def post_data(self, message):
		tokens = message.split(b"\t")
		subject, body = (tokens[0], tokens[0]) if len(tokens) == 1 else (tokens[0], tokens[1])
		return 'message', self.url, self.data + b'&subject=' + subject + b'&status=' + body

	@staticmethod
	def load_composer():
		if hasattr(MessageTarget, 'composer'):
			return MessageTarget.composer

		text = Facebook.load('Loading message composer', 'https://www.facebook.com/ajax/messaging/composer.php?__a=1')

		form = re.search(b'(?s)form[^>]+gigaboxx.*?/form>', text)
		if form:
			MessageTarget.composer = Facebook.parse_input_tags(form.group())
			return MessageTarget.composer

		print('Could not find message form. This script is probably broken.')
		exit(-1)

class CommentTarget(Target):
	def __init__(self, opts):
		super().__init__(opts)

		page = Facebook.load('Loading url %u', opts[1])

		form = re.search(b'(?s)form[^>]+modify.php.*?/form>', page)
		if form:
			self.url = 'https://www.facebook.com/ajax/ufi/modify.php?__a=1'
			self.data = Facebook.parse_input_tags(form.group())
		else:
			print("Could not find a comment form. Are you sure you can comment on this page? If")
			print("so, then Facebook may have been updated in a way that breaks this script.")
			exit(-1)

	def like_item(self, response):
		form = re.search(b'like_comment_id\\[\\d+\\]', response)
		if form:
			data = form.group() + b'=&' + self.data
			Facebook.parse_response(Facebook.load('Sending like request #%c', self.url, data, 'like'))
		else:
			print("Could not find item to like.")

	def delete_item(self, response):
		form = re.search(b'delete\\[\\d+\\]', response)
		if form:
			data = form.group() + b'=&confirmed=1&' + self.data
			Facebook.parse_response(Facebook.load('Sending delete request #%c', self.url, data, 'delete'))
		else:
			print("Could not find item to delete.")

	def post_data(self, message):
		return 'comment', self.url, b'comment=1&' + self.data + b'&add_comment_text=' + message + b'&add_comment_text_text=' + message

class WallTarget(Target):
	def __init__(self, opts):
		super().__init__(opts)

		wall = Facebook.load('Loading wall %u', Facebook.lookup_wall_url(opts[1]))

		form = re.search(b'(?s)form[^>]+updatestatus.php.*?/form>', wall)
		if form:
			self.url = 'https://www.facebook.com/ajax/updatestatus.php?__a=1'
			self.data = Facebook.parse_input_tags(form.group())
		else:
			print("Could not find a write on wall form. Are you sure you can post on this wall?")
			print("If so, then Facebook may have been updated in a way that breaks this script.")
			exit(-1)

	def like_item(self, response):
		form = re.search(b'(?s)form[^>]+modify.php.*?/form>', response)
		if form:
			url = 'https://www.facebook.com/ajax/ufi/modify.php?__a=1'
			data = b'like=&' + Facebook.parse_input_tags(form.group())
			Facebook.parse_response(Facebook.load('Sending like request #%c', url, data, 'like'))
		else:
			print("Could not find item to like.")

	def delete_item(self, response):
		form = re.search(b'minifeed.php\\?([^"]*?)\\\\*"', response)
		if form:
			url = 'https://www.facebook.com/ajax/minifeed.php?__a=1'
			data = parse.unquote(form.group(1).replace(b'\\\\u', b'\u').replace(b'&amp;', b'&').decode("unicode-escape")).encode("utf-8") + b'&confirmed=1&fb_dtsg=' + re.search(b'input[^>]*name=\\\\*"fb_dtsg\\\\*"[^>]*value=\\\\*"([^"]*?)\\\\*"', response).group(1)
			Facebook.parse_response(Facebook.load('Sending delete request #%c', url, data, 'delete'))
		else:
			print("Could not find item to delete.")

	def post_data(self, message):
		return 'wall post', self.url, self.data + b'&xhpc_message_text=' + message + b'&xhpc_message=' + message

class LikeTarget(Target):
	def __init__(self, opts):
		super().__init__(opts)

		halt = 'halt' in opts
		relike = 'relike' in opts
		restrict = None
		for opt in opts:
			if opt.startswith('restrict:'):
				restrict = opt[9:]
		limited = 'limited' in opts and not restrict

		wall = Facebook.load('Loading wall posts', Facebook.lookup_wall_url(opts[1]))

		if restrict:
			allowed_ids = { Facebook.find_fbid(wall), Facebook.lookup_load_fbid(restrict) }
		if limited:
			allowed_ids = { Facebook.fbid, Facebook.find_fbid(wall), None }

		while True:
			for post in re.finditer(b'(?s)li [^>]+(?:(?:stream_story|&quot;actrs&quot;:(?:&quot;)?(?P<actrs>[0-9]+)?|&quot;targets&quot;:(?:&quot;)?(?P<targets>[0-9]+)?)[^>]+){3}>.*?feedback_params.*?(?:(?:u003cli.*?/li|(?P<unlike>unlike)).*?)*/li', wall): # i'm so sorry
				story_ids = set(post.group('actrs', 'targets'))
				if restrict and allowed_ids != story_ids:
					continue
				if limited and not allowed_ids >= story_ids:
					continue

				if post.group('unlike'):
					if halt:
						return
					elif not relike:
						continue

				url = 'https://www.facebook.com/ajax/ufi/modify.php?__a=1'
				data = b'like=&' + Facebook.parse_input_tags(post.group())
				Facebook.parse_response(Facebook.load('Sending like request #%c', url, data, 'like'))

			next = re.search(b'PersonalProfileWallTabPagelet\\\\", (.*?), ', wall)
			if next:
				wall = Facebook.load('Loading wall posts', 'https://www.facebook.com/ajax/pagelet/generic.php/PersonalProfileWallTabPagelet?__a=1&ajaxpipe=1&data=' + parse.quote(next.group(1).replace(b'\\', b'').decode()))
			else:
				break

	def post(self, message):
		pass

@atexit.register
def exit_func():
	try:
		if not args.batch:
			input("\nPress enter to exit...")
	except KeyboardInterrupt:
		pass

try:
	parser = argparse.ArgumentParser()
	parser.add_argument('-b', '--batch', action='store_true', help='disable wait on exit')
	parser.add_argument('-n', '--no-browser', action='store_true', help='don\'t automatically open browser for captchas')
	parser.add_argument('-c', '--config', default='wall.txt', help='where to find settings (default: %(default)s)')
	args = parser.parse_args()

	Facebook.open_browser = not args.no_browser
	Facebook.run(args.config)
except KeyboardInterrupt:
	args.batch = True
